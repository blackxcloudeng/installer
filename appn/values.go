package appn

type ValuesHolder struct {
	AppnNameSpace string
	ProxyAgent    ProxyAgent
	AppnScope     AppnScope
}

type ProxyAgent struct {
	Port      string
	ProxyPort string
	Image     string
	HaProxyIp string
}

type AppnScope struct {
	Image           string
	AccountName     string
	AccountId       string
	Appname         string
	AppId           string
	AppsoneIp       string
	AppsonePort     string
	AppsoneUserName string
	AppsonePassword string
	AppsoneToken    string
	AppsoneVer      string
	AppsoneClientID string
	AppsoneGrantType string
}

type TemplateValues struct{
	Values ValuesHolder
}
