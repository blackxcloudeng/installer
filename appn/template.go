package appn

import (
	"bytes"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"

)

var templatePath = "./templates"
var kubernetesResource = make(map[string]string)
var preHooks = make(map[string]string)

func isPreHook(s string) bool {
	return strings.Contains(s, "pre-install")
}

func Execute(v ValuesHolder, dryRun bool) error {
	appnNamespace = v.AppnNameSpace
	tv := TemplateValues{Values: v}
	fmt.Println("TV: ", tv)
	err := filepath.Walk(templatePath, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		fmt.Printf("\n\nFileinfo: path %s\n\n", path)
		templ, err := template.ParseFiles(path)
		if err != nil {
			fmt.Printf("Error in parsing template %s: %s\n", path, err.Error())
		}
		fmt.Printf("templ: %s\n\n", templ.Name())
		var output bytes.Buffer
		err = templ.Execute(&output, tv)
		if err != nil {
			fmt.Printf("Error in executing template %s: %s\n", templ.Name(), err.Error())
		}
		outputstring := output.String()
		if isPreHook(outputstring) {
			if _, ok := preHooks[path]; !ok {
				preHooks[path] = outputstring
			} else {
				fmt.Println("ERROR: resource repeated: ", path)
			}
		} else {

			if _, ok := kubernetesResource[path]; !ok {
				kubernetesResource[path] = outputstring
			} else {
				fmt.Println("ERROR: resource repeated: ", path)
			}
		}
		fmt.Println("Sucess for file: ", path)
		return nil
	})
	if err != nil {
		fmt.Println("Error in executing template: ", err)
		return err
	}

	if dryRun {
		fmt.Println("Prehook")
		printResources(preHooks)
		fmt.Println("kubernetesResource")
		printResources(kubernetesResource)
	} else {
		err = createConfDir()
		if err != nil {
			log.Print("Error in creating conf directory")
			return err
		}
		err = generateConf(preHooks, kubernetesResource)
		if err != nil {
			log.Print("Error in generating conf ")
			return err
		}
		err = createResources(preHooks, kubernetesResource)
		if err != nil {
			log.Print("Error in creating resources")
			return err
		}
	}
	return nil
}

func printResources(m map[string]string) {
	for lKey, lResource := range m {
		fmt.Println("key: ", lKey)
		fmt.Println("resource value")
		fmt.Println(lResource)
	}
}
