package appn

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	"github.com/mitchellh/go-homedir"
)

const confdir = ".installerConfig"
const confFile = "installerConfig"

var appnNamespace string

var confFilePath string

func getConfDir() string {
	if confFilePath == "" {
		homedirname, _ := homedir.Dir()
		confFilePath = fmt.Sprintf("%s/%s/%s.yaml", homedirname, confdir, confFile)
	}
	return confFilePath
}

func createConfDir() error {
	homedirname, _ := homedir.Dir()
	dir := fmt.Sprintf("%s/%s", homedirname, confdir)
	_, err := os.Stat(dir)

	if os.IsNotExist(err) {
		errDir := os.MkdirAll(dir, 0777)
		if errDir != nil {
			log.Print(err)
			return errDir
		}

	}
	confFilePath = fmt.Sprintf("%s/%s/%s.yaml", homedirname, confdir, confFile)
	return nil
}

func generateConf(hook, resources map[string]string) error {
	f, err := os.Create(getConfDir())
	if err != nil {
		log.Printf("Error in creating conf file %s: %s", getConfDir(), err.Error())
		return err
	}
	// This function can be used to figure out what resources got added and removed, they will
	// stored in a conf. On delete the resources which were added and removed will be deleted.
	// For now let's delete all the deployment, daemonset, secrets and services of the namespace
	// and then delete the namespace.
	f.Write([]byte(appnNamespace))
	f.Close()
	return nil
}

func createResources(hook, resources map[string]string) error {
	for _, lYaml := range hook {
		f, err := os.Create("temp.yaml")
		if err != nil {
			log.Printf("Error in creating conf file temp file: %s", err.Error())
			return err
		}
		f.Write([]byte(lYaml))
		f.Close()
		err = kubectl("apply", "-f", "temp.yaml")
		if err != nil {
			log.Printf("Error in applying conf")
			deleteAll(appnNamespace)
			return err
		}
	}
	for _, lYaml := range resources {
		f, err := os.Create("temp.yaml")
		if err != nil {
			log.Printf("Error in creating conf file temp file: %s", err.Error())
			return err
		}
		f.Write([]byte(lYaml))
		f.Close()
		err = kubectl("apply", "-f", "temp.yaml")
		if err != nil {
			log.Printf("Error in applying conf")
			deleteAll(appnNamespace)
			return err
		}
	}
	return nil
}

func kubectl(command string, values ...string) error {
	cmd := exec.Command("kubectl", append([]string{command}, values...)...)
	fmt.Println(cmd)
	cmdOutput := &bytes.Buffer{}
	cmd.Stdout = cmdOutput
	err := cmd.Run()
	if err != nil {
		os.Stderr.WriteString(err.Error())
		log.Printf("Error in executing command %s and values %v", command, values)
		return err
	}
	fmt.Println("output is")
	fmt.Print(string(cmdOutput.Bytes()))
	return nil
}

func deleteAll(ns string) {
	/*
		resourceTypes := []string{"deployment", "daemonset", "service", "secret"}
		for _, resource := range resourceTypes {
			fmt.Println("deleting ", resource)
			err := kubectl("delete", "--all", resource, "--namespace", ns)
			if err != nil {
				log.Printf("Error in deleting %s", resource)
			}
		}
	*/
	err := kubectl("delete", "namespace", ns)
	if err != nil {
		log.Printf("Error in deleting namespace %s", ns)
	}

}

func DeleteResource() error {
	namespace, err := ioutil.ReadFile(getConfDir())
	if err != nil {
		log.Print("Error namespace not found in conf, skipping the uninstall", err)
		return err
	}
	if len(namespace) == 0 {
		log.Print("No namespace found in conf, skipping the uninstall")
		return errors.New("Namespace not found")
	}
	deleteAll(string(namespace))
	return nil
}

/*
func main(){
	kubectl("get", "pods")
	deleteAll("appnomic")
}
*/
